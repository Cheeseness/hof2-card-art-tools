# Hand of Fate 2 Card Art Tools

A collection of tools to aid the creation of custom card art for Hand of Fate 2 mods. Currently comprising of a Gimp script-fu script that allows card art to be previewed as an approximation of how it will appear in-game.


## Setup

### Requirements

The script was written for Gimp 2.10 and may not behave as expected with older versions.

### Installation steps

  1. Download or clone this repository into your Gimp scripts folder

For more information, see [Section 2.2 of the Gimp documentation](https://docs.gimp.org/en/install-script-fu.html).


## Usage

The script currently contains three options for previewing card art, which can be accessed via the **Image->Hand of Fate 2** menu.

  - **Normal Card Preview**: Displays a preview of what the current image would look like with regular card colouring
  - **Brimstone Card Preview**: Displays a preview of what the current image would look like with Brimstone card colouring
  - **Platinum Card Preview**:  Displays a preview of what the current image would look like with Platinum card colouring

For the purposes of readability of elements that are animated in Hand of Fate 2, the above functions use solid colours with higher levels of contrast than are present in the game.

For more information on making custom card art and other content for Hand of Fate 2, visit the [community modding wiki](https://hof2modwiki.cheesetalks.net/).


## Licence

This project is made available under the GPLv3. Please see LICENCE for full terms.
