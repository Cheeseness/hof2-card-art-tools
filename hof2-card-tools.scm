; A script for previewing Hand of Fate 2 card art effects

; Copyright 2022, Josh "Cheeseness" Bush
;
; This program is free software: you can redistribute it and/or modify it under
; the terms of the GNU General Public License as published by the Free Software
; Foundation, either version 3 of the License, or (at your option) any later
; version.
;
; This program is distributed in the hope that it will be useful, but WITHOUT
; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
; details.
;
; You should have received a copy of the GNU General Public License along with
; this program. If not, see <https://www.gnu.org/licenses/>.

(define paperColour '(160 144 99))
(define paperColourB '(189 156 86))
(define paperColourP '(189 169 115))
(define lavaColour '(255 0 0))
(define foilColour '(145 155 170))
(define inkColour '(32 32 32))
(define cloudColour '(255 255 0))
(define shinyColour '(255 255 255))

(define (script-fu-hof2-normal-preview originalImage)
	(let* (
		;Flaten and decompose our original image
		(imageWidth (car (gimp-image-width originalImage)))
		(imageHeight (car (gimp-image-height originalImage)))
		(tempImage (car (gimp-image-duplicate originalImage)))
		(tempDrawable (car (gimp-image-merge-visible-layers tempImage 1)))
		(oldForeground (car (gimp-context-get-foreground)))
		)
	(begin
		;Create new layers
		(define paperLayer (car (gimp-layer-new tempImage imageWidth imageHeight RGB-IMAGE "Paper" 100 LAYER-MODE-NORMAL)))
		(define inkLayer (car (gimp-layer-new tempImage imageWidth imageHeight RGB-IMAGE "Ink" 100 LAYER-MODE-NORMAL)))

		;Insert created layers into image
		(gimp-image-insert-layer tempImage paperLayer 0 0)
		(gimp-image-insert-layer tempImage inkLayer 0 0)

		;Fill layers with appropriate colours
		(gimp-context-set-foreground paperColour)
		(gimp-drawable-fill paperLayer 0)
		(gimp-context-set-foreground inkColour)
		(gimp-drawable-fill inkLayer 0)

		;Add ink mask based on inverse of alpha channel
		(define tempMask (car (gimp-layer-create-mask inkLayer 0)))
		(gimp-drawable-invert tempDrawable TRUE)
		(gimp-image-set-active-layer tempImage tempDrawable)
		(gimp-edit-copy tempDrawable)
		(gimp-layer-add-mask inkLayer tempMask)
		(define floating-sel (car (gimp-edit-paste tempMask TRUE)))
		(gimp-floating-sel-anchor floating-sel)

		;Remove redundant layers
		(gimp-image-remove-layer tempImage tempDrawable)

		;Display new image
		(gimp-display-new tempImage)
		(gimp-displays-flush)
		(gimp-image-clean-all tempImage)

		;Restore previous foreground colour
		(gimp-context-set-foreground oldForeground)
	)
))

(define (script-fu-hof2-brimstone-preview originalImage)
	(let* (
		;Flaten and decompose our original image
		(imageWidth (car (gimp-image-width originalImage)))
		(imageHeight (car (gimp-image-height originalImage)))
		(tempImage (car (gimp-image-duplicate originalImage)))
		(tempDrawable (car (gimp-image-merge-visible-layers tempImage 1)))
		(decomposedImage (car (plug-in-decompose 1 tempImage tempDrawable "RGBA" 1)))
		(oldForeground (car (gimp-context-get-foreground)))
		)
	(begin
		;Make our decomposed image RGB
		(gimp-image-convert-rgb decomposedImage)


		;Create new layers and layer groups
		(define paperLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Paper" 100 LAYER-MODE-NORMAL)))
		(define lavaLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Lava" 100 LAYER-MODE-NORMAL)))
		(define cloudLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Glowing" 100 LAYER-MODE-LINEAR-LIGHT)))
		(define inkLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Ink" 100 LAYER-MODE-NORMAL)))
		(define inkCloudGroup (car (gimp-layer-group-new decomposedImage)))
		(gimp-item-set-name inkCloudGroup "Ink/Glow Group")

		;Setup variables for channels
		(define redChannel (car (gimp-image-get-layer-by-name decomposedImage "red")))
		(define greenChannel (car (gimp-image-get-layer-by-name decomposedImage "green")))
		(define blueChannel (car (gimp-image-get-layer-by-name decomposedImage "blue")))
		(define alphaChannel (car (gimp-image-get-layer-by-name decomposedImage "alpha")))

		;Set colour channels to darken only and hide alpha
		(gimp-layer-set-mode redChannel LAYER-MODE-DARKEN-ONLY)
		(gimp-layer-set-mode greenChannel LAYER-MODE-DARKEN-ONLY)
		(gimp-layer-set-mode blueChannel LAYER-MODE-DARKEN-ONLY)
		(gimp-item-set-visible alphaChannel 0)

		;Copy what we have here for use later as the lava mask
		(gimp-edit-copy-visible decomposedImage)
		(gimp-item-set-visible alphaChannel 1)

		;Insert created layers into image
		(gimp-image-insert-layer decomposedImage paperLayer 0 0)
		(gimp-image-insert-layer decomposedImage inkCloudGroup 0 0)
		(gimp-image-insert-layer decomposedImage lavaLayer 0 0)
		(gimp-image-insert-layer decomposedImage cloudLayer inkCloudGroup 0)
		(gimp-image-insert-layer decomposedImage inkLayer inkCloudGroup 1)


		;Fill layers with appropriate colours
		(gimp-context-set-foreground paperColourB)
		(gimp-drawable-fill paperLayer 0)
		(gimp-context-set-foreground lavaColour)
		(gimp-drawable-fill lavaLayer 0)
		(gimp-context-set-foreground cloudColour)
		(gimp-drawable-fill cloudLayer 0)
		(gimp-context-set-foreground inkColour)
		(gimp-drawable-fill inkLayer 0)

		;Add lava mask based on darken only combination of all colour channels
		(define tempMask (car (gimp-layer-create-mask lavaLayer 0)))
		(gimp-layer-add-mask lavaLayer tempMask)
		(define floating-sel (car (gimp-edit-paste tempMask TRUE)))
		(gimp-floating-sel-anchor floating-sel)

		;Add ink/cloud mask based on blue channel
		(set! tempMask (car (gimp-layer-create-mask inkCloudGroup 0)))
		(gimp-image-set-active-layer decomposedImage blueChannel)
		(gimp-edit-copy blueChannel)
		(gimp-layer-add-mask inkCloudGroup tempMask)
		(define floating-sel (car (gimp-edit-paste tempMask TRUE)))
		(gimp-floating-sel-anchor floating-sel)

		;Add ink mask based on inverse of alpha channel
		(set! tempMask (car (gimp-layer-create-mask cloudLayer 0)))
		(gimp-image-set-active-layer decomposedImage alphaChannel)
		(gimp-edit-copy alphaChannel)
		(gimp-layer-add-mask cloudLayer tempMask)
		(define floating-sel (car (gimp-edit-paste tempMask TRUE)))
		(gimp-floating-sel-anchor floating-sel)

		;Remove redundant layers
		(gimp-image-remove-layer decomposedImage redChannel)
		(gimp-image-remove-layer decomposedImage greenChannel)
		(gimp-image-remove-layer decomposedImage blueChannel)
		(gimp-image-remove-layer decomposedImage alphaChannel)

		;Display new image
		(gimp-display-new decomposedImage)
		(gimp-displays-flush)
		(gimp-image-clean-all decomposedImage)

		;Restore previous foreground colour
		(gimp-context-set-foreground oldForeground)
	)
))

(define (script-fu-hof2-platinum-preview originalImage)
	(let* (
		;Flaten and decompose our original image
		(imageWidth (car (gimp-image-width originalImage)))
		(imageHeight (car (gimp-image-height originalImage)))
		(tempImage (car (gimp-image-duplicate originalImage)))
		(tempDrawable (car (gimp-image-merge-visible-layers tempImage 1)))
		(decomposedImage (car (plug-in-decompose 1 tempImage tempDrawable "RGBA" 1)))
		(oldForeground (car (gimp-context-get-foreground)))
		)
	(begin
		;Make our decomposed image RGB
		(gimp-image-convert-rgb decomposedImage)


		;Create new layers and layer groups
		(define paperLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Paper" 100 LAYER-MODE-NORMAL)))
		(define foilLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Foil" 100 LAYER-MODE-NORMAL)))
		(define shinyLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Glowing" 100 LAYER-MODE-LINEAR-LIGHT)))
		(define inkLayer (car (gimp-layer-new decomposedImage imageWidth imageHeight RGB-IMAGE "Ink" 100 LAYER-MODE-NORMAL)))
		(define inkShinyGroup (car (gimp-layer-group-new decomposedImage)))
		(gimp-item-set-name inkShinyGroup "Ink/Shiny Group")

		;Setup variables for channels
		(define redChannel (car (gimp-image-get-layer-by-name decomposedImage "red")))
		(define greenChannel (car (gimp-image-get-layer-by-name decomposedImage "green")))
		(define blueChannel (car (gimp-image-get-layer-by-name decomposedImage "blue")))
		(define alphaChannel (car (gimp-image-get-layer-by-name decomposedImage "alpha")))

		;Set colour channels to darken only and hide alpha
		(gimp-layer-set-mode redChannel LAYER-MODE-DARKEN-ONLY)
		(gimp-layer-set-mode greenChannel LAYER-MODE-DARKEN-ONLY)
		(gimp-layer-set-mode blueChannel LAYER-MODE-DARKEN-ONLY)
		(gimp-item-set-visible alphaChannel 0)

		;Copy what we have here for use later as the lava mask
		(gimp-edit-copy-visible decomposedImage)
		(gimp-item-set-visible alphaChannel 1)

		;Insert created layers into image
		(gimp-image-insert-layer decomposedImage paperLayer 0 0)
		(gimp-image-insert-layer decomposedImage inkShinyGroup 0 0)
		(gimp-image-insert-layer decomposedImage foilLayer 0 0)
		(gimp-image-insert-layer decomposedImage shinyLayer inkShinyGroup 0)
		(gimp-image-insert-layer decomposedImage inkLayer inkShinyGroup 1)


		;Fill layers with appropriate colours
		(gimp-context-set-foreground paperColourP)
		(gimp-drawable-fill paperLayer 0)
		(gimp-context-set-foreground foilColour)
		(gimp-drawable-fill foilLayer 0)
		(gimp-context-set-foreground shinyColour)
		(gimp-drawable-fill shinyLayer 0)
		(gimp-context-set-foreground inkColour)
		(gimp-drawable-fill inkLayer 0)

		;Add foil mask based on darken only combination of all colour channels
		(define tempMask (car (gimp-layer-create-mask foilLayer 0)))
		(gimp-layer-add-mask foilLayer tempMask)
		(define floating-sel (car (gimp-edit-paste tempMask TRUE)))
		(gimp-floating-sel-anchor floating-sel)

		;Add shiny mask based on blue channel
		(set! tempMask (car (gimp-layer-create-mask inkShinyGroup 0)))
		(gimp-image-set-active-layer decomposedImage blueChannel)
		(gimp-edit-copy blueChannel)
		(gimp-layer-add-mask inkShinyGroup tempMask)
		(define floating-sel (car (gimp-edit-paste tempMask TRUE)))
		(gimp-floating-sel-anchor floating-sel)

		;Add ink/shiny mask based on inverse of alpha channel
		(set! tempMask (car (gimp-layer-create-mask shinyLayer 0)))
		(gimp-image-set-active-layer decomposedImage alphaChannel)
		(gimp-edit-copy alphaChannel)
		(gimp-layer-add-mask shinyLayer tempMask)
		(define floating-sel (car (gimp-edit-paste tempMask TRUE)))
		(gimp-floating-sel-anchor floating-sel)

		;Remove redundant layers
		(gimp-image-remove-layer decomposedImage redChannel)
		(gimp-image-remove-layer decomposedImage greenChannel)
		(gimp-image-remove-layer decomposedImage blueChannel)
		(gimp-image-remove-layer decomposedImage alphaChannel)

		;Display new image
		(gimp-display-new decomposedImage)
		(gimp-displays-flush)
		(gimp-image-clean-all decomposedImage)

		;Restore previous foreground colour
		(gimp-context-set-foreground oldForeground)
	)
))
(script-fu-register
	"script-fu-hof2-normal-preview"				;function name
	"_Normal Card Preview"						;menu label
	"Creates a preview of how Hand of Fate 2\
card art will look when imported into\
the game."										;Description
	"Josh \"Cheeseness\" Bush"
	"Copyright 2022, Josh \"Cheeseness\" Bush"
	"2022-10-20"
	""											; image type??
	SF-IMAGE		"Image"		0
)
(script-fu-register
	"script-fu-hof2-brimstone-preview"			;function name
	"_Brimstone Card Preview"					;menu label
	"Creates a preview of how Hand of Fate 2\
card art will look when imported into\
the game."										;Description
	"Josh \"Cheeseness\" Bush"
	"Copyright 2022, Josh \"Cheeseness\" Bush"
	"2022-10-20"
	"RGBA"										; image type??
	SF-IMAGE		"Image"		0
)
(script-fu-register
	"script-fu-hof2-platinum-preview"			;function name
	"_Platinum Card Preview"						;menu label
	"Creates a preview of how Hand of Fate 2\
card art will look when imported into\
the game."										;Description
	"Josh \"Cheeseness\" Bush"
	"Copyright 2022, Josh \"Cheeseness\" Bush"
	"2022-10-20"
	"RGBA"										; image type??
	SF-IMAGE		"Image"		0
)
(gimp-plugin-menu-branch-register "<Image>/Image" "_Hand of Fate 2")
(script-fu-menu-register "script-fu-hof2-normal-preview" "<Image>/Image/Hand of Fate 2")
(script-fu-menu-register "script-fu-hof2-brimstone-preview" "<Image>/Image/Hand of Fate 2")
(script-fu-menu-register "script-fu-hof2-platinum-preview" "<Image>/Image/Hand of Fate 2")

